$(document).ready(function() {
    var dateline = $('#calendar_cont').data('dateline') != '' ? $('#calendar_cont').data('dateline') : '';
    $.post(archiveUrl + dateline, function(cal) {
        $('#calendar_cont').html(cal);
    });

    $(document).on('click', '#calendar_cont a.prev, #calendar_cont a.next', function() {
        $.post($(this).attr('href'), function(cal) {
            $('#calendar_cont').html(cal);
        });
        return false;
    });

    $(document).on('click', '#calendar_cont a.prevyear, #calendar_cont a.nextyear', function() {
        $.post($(this).attr('href'), function(cal) {
            $('#calendar_cont').html(cal);
        });
        return false;
    });

});