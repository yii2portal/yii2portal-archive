<?php

namespace yii2portal\archive;

use Yii;

class Module extends \yii2portal\core\Module
{
    public $controllerNamespace = 'yii2portal\archive\controllers';

}