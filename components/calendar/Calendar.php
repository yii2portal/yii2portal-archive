<?php


namespace yii2portal\archive\components\calendar;

use Yii;
use yii\web\View;
use yii2portal\core\components\Widget;
use yii2portal\poll\models\Poll;


class Calendar extends Widget
{

    public function insert($view)
    {

        $urlPath = Yii::$app->getModule('structure')->getPageByModule('archive')->urlPath;
        $this->view->registerJs("var archiveUrl = '{$urlPath}c/';", View::POS_BEGIN);


        return $this->render($view, [

        ]);

    }

}