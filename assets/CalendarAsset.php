<?php


namespace yii2portal\archive\assets;

use yii\web\AssetBundle;

class CalendarAsset extends AssetBundle
{
    public $sourcePath = '@yii2portal/archive/client';

    public $css = [
    ];
    public $js = [
        'js/calendar.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
