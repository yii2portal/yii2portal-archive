<?php


return [

    [
        'pattern' => "<year:\d{4}>/<month:\d{2}>/<day:\d{2}>",
        'route' => "index/index",
        'defaults' => [
            'page' => 1,
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d'),
        ]
    ],
    [
        'pattern' => "c/<year:\d{4}>/<month:\d{2}>/<day:\d{2}>",
        'route' => "index/calendar",
        'defaults' => [
            'page' => 1,
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d'),
        ]
    ],
    [
        'pattern' => "<year:\d{4}>/<month:\d{2}>/<day:\d{2}>/page_<page:\d+>",
        'route' => "index/index",
        'defaults' => [
            'page' => 3,
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d'),
        ]
    ],
];