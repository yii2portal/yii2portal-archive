<?php

namespace yii2portal\archive\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii2portal\core\controllers\Controller;
use yii2portal\news\models\News;


class IndexController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'only' => ['calendar'],
                'duration' => 60*60*5,
            ],
            [
                'class' => 'yii\filters\HttpCache',
                'only' => ['calendar'],
                'etagSeed' => function ($action, $params) {
                    return serialize([date("d.m.Y")]);
                },
            ],
        ];
    }

    public function actionIndex($structure_id, $year, $month, $day)
    {



        $start = mktime(0, 0, 0, $month, $day, $year);
        $end = mktime(23, 59, 59, $month, $day, $year);

        $query = News::find()
            ->andPublished()
            ->byDatepublic()
            ->byDatepublic($start, $end);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'datepublic' => SORT_DESC
                ]
            ],
        ]);

        if (!$provider->count) {
            throw  new NotFoundHttpException;
        }

        return $this->render('index', [
            'page' => Yii::$app->modules['structure']->getPage($structure_id),
            'provider' => $provider,
            'year' => $year,
            'month' => $month,
            'day' => $day,
        ]);
    }

    public function actionCalendar($structure_id, $year, $month, $day)
    {
        $currMonth = $month;


        $months_tmp = News::find()
            ->andPublished()
            ->orderBy(['datepublic' => SORT_DESC])
            ->select([
                new Expression("FROM_UNIXTIME(min(datepublic), :format) as min_date", [
                    ':format' => '%c_%Y',
                ]),
                new Expression("FROM_UNIXTIME(max(datepublic), :format) as max_date", [
                    ':format' => '%c_%Y',
                ])
            ])
            ->asArray()
            ->one();

        $min_date = explode('_', $months_tmp['min_date']);
        $max_date = explode('_', $months_tmp['max_date']);


        $data = [];
        $query = News::find()
            ->andPublished()
            ->orderBy(['datepublic' => SORT_DESC])
            ->andWhere(new Expression("FROM_UNIXTIME(datepublic,:formatw)=:yearMonth"), [
                ':formatw' => '%Y_%c',
                ':yearMonth' => "{$year}_{$currMonth}",
            ])
            ->select(new Expression("FROM_UNIXTIME(datepublic, :format) as fulldate", [
                ':format' => '%e_%c',
            ]))
            ->groupBy(new Expression("FROM_UNIXTIME(datepublic, :format)", [
                ':format' => '%e_%c',
            ]))
            ->byDatepublic();

        $data_tmp = $query->asArray()->all();

        for ($i = 0; $i < count($data_tmp); $i++) {
            $data[] = $data_tmp[$i]['fulldate'];
        }


        $m = $month;
        $calendar = [
            'title' => $m,
            'weeks' => []
        ];
        $currentMonth = mktime(0, 0, 0, $m, 1, $year);
        $prevMonth = mktime(0, 0, 0, $m-1, 1, $year);
        $fd = date('N', $currentMonth);

        $countDaysPrevMonth = date('t', $prevMonth);

        for ($i = 1; $i < $fd + date('t', $currentMonth); $i++) {
            $val = ($i >= $fd) ? $i - $fd + 1 : $countDaysPrevMonth -$fd +1  +$i;
            $date = ($i - $fd + 1) . "_{$m}";

            $href = Yii::$app->getModule('structure')->getPageByModule('archive')->urlPath . date("Y/m/{$val}", $currentMonth);
            $val_str = (in_array($date, $data, true)) ? Html::a($val, $href) : Html::tag('span', $val);

            $class = array();
            if (($i - $fd + 1) . "_{$m}_{$year}" == date('j_n_Y'))
                $class[] = "today";
            if ($i % 7 == 0 || ($i - 6) % 7 == 0)
                $class[] = "red";

            if (($i - $fd + 1) . "_{$m}_{$year}" == "{$day}_{$month}_{$year}")
                $class[] = "current";
            if (empty($val))
                $class[] = "empty";

            $calendar['weeks'][] = Html::tag('td', $val_str, [
                'class' => $class
            ]);
        }

        $t = ceil(count($calendar['weeks']) / 7);
        $drawTds = ($t * 7) - count($calendar['weeks']);
        for ($i = 1; $i <= $drawTds; $i++) {
            $calendar['weeks'][] = Html::tag('td', "<span>{$i}</span>", [
                'class' => ['empty']
            ]);
        }


        $tvars['calendar'] = $calendar;
        $tvars['curyear'] = $year;
        $tvars['curday'] = $day;
        $tvars['curmonth'] = $currMonth;

        if (mktime(1, 1, 1, $min_date[0], 1, $min_date[1]) <= mktime(1, 1, 1, $currMonth - 1, 1, $year))
            $tvars['prev_date'] = date("n_Y", mktime(1, 1, 1, $currMonth - 1, 1, $year));


        if (mktime(1, 1, 1, $max_date[0], 1, $max_date[1]) >= mktime(1, 1, 1, $currMonth + 1, 1, $year))
            $tvars['next_date'] = date("n_Y", mktime(1, 1, 1, $currMonth + 1, 1, $year));

        $tvars['prev_month'] = date("n", mktime(1, 1, 1, $currMonth - 1, 1, $year));
        $tvars['next_month'] = date("n", mktime(1, 1, 1, $currMonth + 1, 1, $year));
        $tvars['max_date'] = $max_date;
        $tvars['min_date'] = $min_date;

        return $this->renderAjax('calendar', $tvars);

    }

}
